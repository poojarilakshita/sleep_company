import React from 'react'
import './Story.css'
import story_bg from '../Images/Story_bg.png'
// import story_1 from '../Images/Story_1.png'
import story_2 from '../Images/Story_2.png'
// import story_3 from '../Images/Story_3.png'
import story_4 from '../Images/Story_4.png'
import story_5 from '../Images/Story_5.png'
import video_1 from '../Images/Smart_Recliner_Bed.mp4'
import video_2 from '../Images/Sleep_Peacefully.mp4'

const Story = () => {
  return (
    <div>
      <img src={story_bg} alt="" className="bg_3" />
        <p className="head_9">Our story is your story.</p>
      {/* <img src={story_1} alt="" className="story_1" /> */}
      <video className='story_1' width="100%" autoPlay loop muted controlsList="nofullscreen">
        <source src={video_1} type="video/mp4" />
      </video>
<video className='story_3' width="100%" autoPlay loop muted controlsList="nofullscreen">
        <source src={video_2} type="video/mp4" />
      </video>

      <img src={story_2} alt="" className="story_2" />
      {/* <img src={story_3} alt="" className="story_3" /> */}
      <img src={story_4} alt="" className="story_4" />
      <img src={story_5} alt="" className="story_5" />
    </div>
  )
}

export default Story
