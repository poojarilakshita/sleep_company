import React, { useState } from 'react';
import './Description.css'

const Description = () => {
    const [email, setEmail] = useState('');
    const [phone, setPhone] = useState('');

    const handleEmailChange = (e) => {
        setEmail(e.target.value);
    };

    const handlePhoneChange = (e) => {
        setPhone(e.target.value);
    };

    const handleSendClick = () => {
        console.log('Email:', email);
        console.log('Phone Number:', phone);
        setEmail('');
        setPhone('');
    };
    
    const handleIconClick = (socialMedia) => {
        console.log(`Clicked on ${socialMedia} icon`);
    };

    return (
        <div className='more'>
            <div className="products">
                <h1 className="pro">Products</h1>
                <p className='p35' >Matterss <br />
                    Beds <br />
                    Pillows & Cushions <br />
                    Beddings <br />
                    Chairs <br />
                </p>
            </div>
            <div className="support">
                <h1 className="Supp">Support</h1>
                <p className='p35'>Privacy Policy <br />
                    Terms & Conditions <br />
                    FAQ's <br />
                    Warranty Registrations <br />
                    100 Nights Trails <br />
                    Customer Grievance Redressal Policy <br />
                    Best Smartgrid maters in India <br />
                    Locate Us <br />
                </p>
            </div>
            <div className="quick_links">
                <h1 className="links">Quick links</h1>
                <p className='p35' >About Us<br />
                    Reviews <br />
                    Blogs <br />
                    Mattress Size Guide <br />
                    Track Your Order<br />
                    Refer & Earn <br />
                    Offers <br />
                    Interior Connect program <br />
                    Mattress Recommendar <br />
                    Chair Recommendar <br />
                    Career Opportunities <br />
                    Bulk Orders<br />
                    Media Buzz <br />
                </p>
            </div>
            <div className="newsletter">
                <h1 className="newlettes">Newsletter</h1>
                <p className='p35' >Sign up for latest snooz alerts about SmartGrid, sleep tips & amazing discounts.
                </p>
                <input
                    type="text"
                    className="email"
                    placeholder="Email ID"
                    value={email}
                    onChange={handleEmailChange}
                />
                <input
                    type="text"
                    className="phone_no"
                    placeholder="Phone No"
                    value={phone}
                    onChange={handlePhoneChange}
                />
                <button className="send" onClick={handleSendClick}>
                    SEND MY DISCOUNT CODE
                </button>
            </div>
            <div className="contact_3">
                <h1 className="contact_3">Contact Us</h1>
                <p className='cont_3'>care@theSleepcomapny.in<br />
                    +91-9811981911
                </p>
            </div>
            <div className="connect_on">
                <h1 className="connect_on">Connect On</h1>
                <div className="fonts_3">
                    <i
                        className="fa-brands fa-instagram"
                        onClick={() => handleIconClick('Instagram')}
                    ></i>
                    <i
                        className="fa-brands fa-facebook"
                        onClick={() => handleIconClick('Facebook')}
                    ></i>
                    <i
                        className="fa-brands fa-linkedin"
                        onClick={() => handleIconClick('LinkedIn')}
                    ></i>
                    <i className="fa-brands fa-whatsapp"
                        onClick={() => handleIconClick('YouTube')}
                    ></i>
                </div>
            </div>
        </div>
    )
}

export default Description
