import React, { useState } from 'react';
import './Chairs.css';
import chair_1 from '../Images/chair_1.png';
import chair_2 from '../Images/chair_2.png';
import chair_3 from '../Images/chair_3.png';
import chair_4 from '../Images/chair_4.png';
import chair_5 from '../Images/chair_5.png';

const Chairs = () => {
  const [isActiveOfficeChair, setIsActiveOfficeChair] = useState(true);  // Set to true by default
  const [isActiveGamerChair, setIsActiveGamerChair] = useState(false);

  const handleChairClick = (itemName, setIsActive) => {
    console.log(`Clicked on ${itemName}`);
    // Deactivate other chair type
    setIsActiveGamerChair(false);
    setIsActiveOfficeChair(false);
    // Toggle the clicked chair type
    setIsActive(true);
  };

  return (
    <>
      <div>
        <button
          type="button"
          className={`office_chair ${isActiveOfficeChair ? 'active' : ''}`}
          onClick={() => handleChairClick('Office Chair', setIsActiveOfficeChair)}
        >
          OFFICE CHAIR
        </button>
        {isActiveOfficeChair && (
          <div className="chair">
            <img src={chair_1} alt="" className="chair_1" />
            <img src={chair_2} alt="" className="chair_2" />
            <img src={chair_3} alt="" className="chair_3" />
            <img src={chair_4} alt="" className="chair_4" />
            <img src={chair_5} alt="" className="chair_5" />
          </div>
        )}

        <button
          type="button"
          className={`gamer_chair ${isActiveGamerChair ? 'active' : ''}`}
          onClick={() => handleChairClick("Gamer's chair", setIsActiveGamerChair)}
        >
          GAMER'S CHAIR
        </button>
        {isActiveGamerChair && (
          <div className="chair">
            <img src={chair_1} alt="" className="chair_3" />
            <img src={chair_2} alt="" className="chair_4" />
            <img src={chair_3} alt="" className="chair_5" />
            <img src={chair_4} alt="" className="chair_1" />
            <img src={chair_5} alt="" className="chair_2" />
          </div>
        )}

        <div className="buttons_3">
          <button className='shop_now_2' type="button" onClick={(e) => handleChairClick("Shop Now", setIsActiveOfficeChair)}>SHOP NOW</button>
          <button className='shop_now_3' type="button" onClick={(e) => handleChairClick("Shop Now", setIsActiveOfficeChair)}>SHOP NOW</button>
          <button className='shop_now_4' type="button" onClick={(e) => handleChairClick("Shop Now", setIsActiveOfficeChair)}>SHOP NOW</button>
          <button className='shop_now_5' type="button" onClick={(e) => handleChairClick("Shop Now", setIsActiveOfficeChair)}>SHOP NOW</button>
          <button className='shop_now_6' type="button" onClick={(e) => handleChairClick("Shop Now", setIsActiveOfficeChair)}>SHOP NOW</button>
        </div>
      </div>
    </>
  );
};

export default Chairs;
