import React from 'react'
import './Ad.css'
import ad from '../Images/ad.png'
import erg from '../Images/Erg_Smart_2.svg'
import ad_4 from '../Images/ad_4.png'

const Ad = () => {
  return (
    <div className='ad'>
      <img className='ad_1' src={ad} alt="" />
      <img className='ad_4' src={ad_4} alt="" />
      <img className='logo_1' src={erg} alt="" />
      <p className='text'>Smarter Design For Smater You.</p>
    </div>
  )
}

export default Ad
