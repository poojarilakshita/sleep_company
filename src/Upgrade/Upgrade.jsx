import React from 'react'
import './Upgrade.css'
import upgrade from '../Images/Upgrade.png'
import arrow from '../Images/red_right_arrow.png'

const Upgrade = () => {
    const handleClick = () => {
        console.log('Find Your perfect chair clicked!');
      };
  return (
    <div>
      <img src={upgrade} alt="" className="upgrade" />
      <button className="upgrade_button" type="button" onClick={handleClick}>Find Your Perfect Chair</button>
      <img src={arrow} alt="" className="red_arrow" />
    </div>
  )
}

export default Upgrade
