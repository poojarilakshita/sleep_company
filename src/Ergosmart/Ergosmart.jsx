import React from 'react'
import './Ergosmart.css'
import clip from '../Images/erg_1.png'
import clip_2 from '../Images/erg_2.png'
import clip_3 from '../Images/erg_3.png'
import play from '../Images/play.png'
import video_16 from '../Images/Sleep_Peacefully.mp4'



const Ergosmart = () => {
  return (
    <>
    <div className='heads'>
      <h1 className="head_1">WHY ERGOSMART</h1>
    </div>

    <div className="shorts">
    <video className='clip' width="100%" autoPlay loop muted controlsList="nofullscreen">
        <source src={video_16} type="video/mp4" />
      </video>
        {/* <img src={clip} alt="" className="clip" />
        <img src={play} alt="" className="play_1" /> */}
        <p className="head_3">Patented Smartgrid Technology</p>
    </div>

    <div className="shorts_2">
    <img src={clip_2} alt="" className="clip_1" />
    <img src={play} alt="" className="play_2" />
    <p className="head_4">Patented Ergonomic Design</p>

    </div>
    <div className="shorts_3">
    <img src={clip_3} alt="" className="clip_2" />
    <img src={play} alt="" className="play_3" />
    <p className="head_5">Body Adaptive Comfort</p>

    </div>
    </>

  )
}

export default Ergosmart
