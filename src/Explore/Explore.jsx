import React from 'react'
import './Explore.css'
import mesh from '../Images/Mesh.png'
import store from '../Images/Store.png'
import demo from '../Images/Prod_demo.png'
import connect from '../Images/Connect.png'

const Explore = () => {
    const handleClick = (itemName, event) => {
        event.preventDefault();
        console.log(`Clicked on ${itemName}`);
    };
  return (
    <div className='explore'>
        <h1 className='h1'>MORE WAYS TO EXPLORE</h1>
      <img src={mesh} alt="" className="mesh" />
      <img src={store} alt="" className="store" />
      <button className= 'store_button' type="button" onClick={(e) => handleClick("Find Store", e)}>Find Store</button>
      <img src={demo} alt="" className="demo" />
      <button className= 'demo_button' type="button" onClick={(e) => handleClick("Live Product Demo", e)}>Live Product Demo</button>
      <img src={connect} alt="" className="connect" />
      <button className= 'connect_button' type="button" onClick={(e) => handleClick("Connect Now", e)}>Connect Now</button>
    </div>
  )
}

export default Explore
