import React from 'react';
import './Navbar.css';
import logo from '../Images/logo.png';
import line from '../Images/line.png';
import erg_smart from '../Images/ERG_Smart.png'
import { Link } from 'react-router-dom';



const Navbar = () => {
    const handleItemClick = (itemName, event) => {
        event.preventDefault();
        console.log(`Clicked on ${itemName}`);
    };

    return (
        <div className='Navbar'>
            <div className="logo">
                <img className='element_1' src={logo} alt="" />
                <img className='element_2' src={line} alt="" />
                <img className='element_3' src={erg_smart} alt="" />
            </div>
            <div className="nav-items">
                <ul className="navbar-nav" style={{ listStyle: 'none', display: 'flex', gap: '' }}>
                    <li className="mattresses">
                        <i className="fa-solid fa-chevron-down"></i>
                        <Link to="/" onClick={(e) => handleItemClick('Mattresses', e)}>Mattresses</Link>
                        <div className="dropdown-box">
                            <li className="Ortho-Range">
                            <Link to="/Ortho-range">Ortho</Link>
                                <div className="ortho-ranges">
                                    <li className="Smart-Ortho">
                                        <Link to="/" onClick={(e) => handleItemClick('Smart-Ortho', e)}>Smart Ortho</Link>
                                    </li>
                                    <li className="Smart-Ortho-Pro">
                                        <Link to="/" onClick={(e) => handleItemClick('Smart-Ortho-Pro', e)}>Smart Ortho Pro</Link>
                                    </li>
                                    <li className="Smart-Ortho-Royal">
                                        <Link to="/" onClick={(e) => handleItemClick('Smart-Ortho-Royal', e)}>Smart Ortho Royal</Link>
                                    </li>
                                    <li className="Smart-Ortho-Hybrid">
                                        <Link to="/" onClick={(e) => handleItemClick('Smart-Ortho-Hybrid', e)}>Smart Ortho Hybrid</Link>
                                    </li>
                                    <li className="Comparison_19">
                                        <Link to="/" onClick={(e) => handleItemClick('Comparison', e)}>Comparison</Link>
                                    </li>
                                </div>
                            </li>

                            <li className="Luxe-Range">
                                <Link to="/Smart-Luxe">Luxe Range</Link>
                                <div className="luxe-ranges">
                                    <li className="Smart-Luxe">
                                        <Link to="/" onClick={(e) => handleItemClick('Smart-Luxe', e)}>Smart Luxe</Link>
                                    </li>
                                    <li className="Smart-Luxe-Hybrid">
                                        <Link to="/" onClick={(e) => handleItemClick('Smart-Luxe-Hybrid', e)}>Smart Luxe Hybrid</Link>
                                    </li>
                                    <li className="Smart-Luxe-Royal">
                                        <Link to="/" onClick={(e) => handleItemClick('Smart-Luxe-Royal', e)}>Smart Luxe Royal</Link>
                                    </li>
                                    <li className="Find-your-Perfect-Mattress">
                                        <Link to="/" onClick={(e) => handleItemClick('Find-your-Perfect-Mattress', e)}>Find your Perfect Mattress</Link>
                                    </li>
                                    <li className="All-Mattress">
                                        <Link to="/" onClick={(e) => handleItemClick('All-Mattress', e)}>All Mattress</Link>
                                    </li>
                                </div>
                            </li>

                        </div>
                    </li>
                    <li className="recliner-bed">
                        <i className="fa-solid fa-chevron-down"></i>
                        <Link to="/" onClick={(e) => handleItemClick('Recliner Bed', e)}>Recliner Bed</Link>
                        <div className="dropdown-box">
                            <li className="Ortho-Range">
                                <Link to="/" onClick={(e) => handleItemClick('Recline', e)}>Bed</Link>
                            </li>
                        </div>
                    </li>
                    <li className="chairs">
                        <i className="fa-solid fa-chevron-down"></i>
                        <Link to="/" onClick={(e) => handleItemClick('Chairs', e)}>Chairs</Link>
                        <div className="dropdown-box">
                            <li className="Ortho-Range">
                                <Link to="/" onClick={(e) => handleItemClick('Recline', e)}>Bed</Link>
                            </li>
                        </div>
                    </li>
                    <li className="bundles">
                        <i className="fa-solid fa-chevron-down"></i>
                        <Link to="/" onClick={(e) => handleItemClick('Bundles', e)}>Bundles</Link>
                        <div className="dropdown-box">
                            <li className="Ortho-Range">
                                <Link to="/" onClick={(e) => handleItemClick('Recline', e)}>Bed</Link>
                            </li>
                        </div>
                    </li>
                    <li className="pillows">
                        <i className="fa-solid fa-chevron-down"></i>
                        <Link to="/" onClick={(e) => handleItemClick('Pillows', e)}>Pillows</Link>
                        <div className="dropdown-box">
                            <li className="Ortho-Range">
                                <Link to="/" onClick={(e) => handleItemClick('Recline', e)}>Bed</Link>
                            </li>
                        </div>
                    </li>
                    <li className="bedding">
                        <i className="fa-solid fa-chevron-down"></i>
                        <Link to="/" onClick={(e) => handleItemClick('Bedding', e)}>Bedding</Link>
                        <div className="dropdown-box">
                            <li className="Ortho-Range">
                                <Link to="/" onClick={(e) => handleItemClick('Recline', e)}>Bed</Link>
                            </li>
                        </div>
                    </li>
                </ul>
            </div>
            <div className="font_2">
                <i className="fa-solid fa-magnifying-glass" onClick={(e) => handleItemClick('Search', e)}></i>
                <i className="fa-solid fa-cart-shopping" onClick={(e) => handleItemClick('Cart', e)}></i>
            </div>
            <div className='button'>
                <button type="button" className="btn btn-primary" onClick={(e) => handleItemClick('Find Store', e)}>          Find Store</button>
                <i className="fa-solid fa-store"></i>
            </div>
        </div>
    );
};

export default Navbar;
