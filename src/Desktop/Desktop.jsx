import React from 'react'
import './Desktop.css'
import Header from '../Header/Header'
import Navbar from '../Navbar/Navbar'
import Ad from '../Ad/Ad'
import Sales from '../Sales/Sales'
import Shop_now from '../Shop_now/Shop_now'
import Chairs from '../Chairs/Chairs'
import Compare from '../Compare/Compare'
import Ergosmart from '../Ergosmart/Ergosmart'
import Explore from '../Explore/Explore'
import Best_Seller from '../Best_Seller/Best_Seller'
import Review from '../Review/Review'
import Upgrade from '../Upgrade/Upgrade'
import Story from '../Story/Story'
import Blogs from '../Blogs/Blogs'
import FAQ from '../FAQ/FAQ'
import Description from '../Description/Description'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Ortho from '../Ortho/Ortho'
import Luxe from '../Luxe/Luxe'


const Desktop = () => {
  return (
    <Router>
      <div className="desktop">
        <Header />
        <Navbar />
        <Routes>
          <Route exact path="/" element={
          <>
          <Navbar/>
          <Ad />
          <Sales />
          <Shop_now />
          <Chairs />
          <Compare />
          <Ergosmart />
          <Explore />
          <Best_Seller />
          <Review />
          <Upgrade />
          <Story />
          <Blogs />
          <FAQ />
          <Description />
          </>} 
          />
          <Route exact path="/Ortho-range" element={<Ortho/>} />
          <Route exact path="/Smart-Luxe" element={<Luxe />} />
        </Routes>
        
      </div>
    </Router>
  );
};

export default Desktop;