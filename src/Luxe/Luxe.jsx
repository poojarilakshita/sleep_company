import React from 'react'
import './Luxe.css'
import chair_11 from '../Images/chair_1.png';
import chair_21 from '../Images/chair_2.png';
import chair_31 from '../Images/chair_3.png';
import chair_41 from '../Images/chair_4.png';
import chair_51 from '../Images/chair_5.png';

const Luxe = () => {
  return (
    <div className='luxe-page'>
            <img src={chair_11} alt="" className="chair_11" />
            <img src={chair_21} alt="" className="chair_21" />
            <img src={chair_31} alt="" className="chair_31" />
            <img src={chair_41} alt="" className="chair_41" />
            <img src={chair_51} alt="" className="chair_51" />
    </div>
  )
}

export default Luxe
