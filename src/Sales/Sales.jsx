import React from 'react';
// import React, { useState, useEffect } from 'react';
import './Sales.css'
import bank from '../Images/ICIC_Bank.png'
import line_1 from '../Images/line.png'
import certificate from '../Images/BIFMA.png'
import warranty from '../Images/Warranty.png'
import shipping from '../Images/Free_Shipping.png'
import install from '../Images/Free Installation.png'
import policy from '../Images/Policy.png'
import time from '../Images/timer.png'

const Sales = () => {

  // const [countdown, setCountdown] = useState(10); 

  // useEffect(() => {
  //   const timer = setInterval(() => {
  //     setCountdown((prevCountdown) => (prevCountdown > 0 ? prevCountdown - 1 : 0));
  //   }, 1000);

  //   return () => clearInterval(timer);
  // }, []);

  // const formatTime = (seconds) => {
  //   const hours = Math.floor(seconds / 3600);
  //   const remainingMinutes = Math.floor((seconds % 3600) / 60);
  //   const remainingSeconds = seconds % 60;

  //   const formattedHours = String(hours).padStart(2, '0');
  //   const formattedMinutes = String(remainingMinutes).padStart(2, '0');
  //   const formattedSeconds = String(remainingSeconds).padStart(2, '0');

  //   return `${formattedHours}:${formattedMinutes}:${formattedSeconds}`;
  // };

  return (
    <>
      <div className='container'>
        <p className="sale">BLACK FRIDAY SALE</p>
        <p className="off">UPTO 40% OFF</p>
        <img src={bank} alt="" className="bank" />
        <img src={line_1} alt="" className="line_1" />
        <div>
          {/* <p className='timer'>SALE ENDS IN</p> */}
          {/* <p className='time'>{formatTime(countdown)}</p> */}
          {/* <p className="format">DAY HOURS MINUTES</p> */}
          <img src={time} alt="" className="format_3" />
        </div>
      </div>
      <div className="container_1">
        <img src={certificate} alt="" className="certificate" />
        <img src={warranty} alt="" className="warranty" />
        <img src={shipping} alt="" className="shipping" />
        <img src={install} alt="" className="install" />
        <img src={policy} alt="" className="policy" />
      </div>
    </>
  );
};

export default Sales;