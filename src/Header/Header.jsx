import React from 'react'
import './Header.css'

const Header = () => {
    const handleIconClick = (socialMedia) => {
      console.log(`Clicked on ${socialMedia} icon`);
    };
  
    return (
      <div className="box">
        <div className="text">
          5% off on UPI and Credit Card up to 1500
        </div>
        <div className="fonts">
          <i
            className="fa-brands fa-instagram"
            onClick={() => handleIconClick('Instagram')}
          ></i>
          <i
            className="fa-brands fa-facebook"
            onClick={() => handleIconClick('Facebook')}
          ></i>
          <i
            className="fa-brands fa-linkedin"
            onClick={() => handleIconClick('LinkedIn')}
          ></i>
          <i
            className="fa-brands fa-youtube"
            onClick={() => handleIconClick('YouTube')}
          ></i>
        </div>
      </div>
    );
  };
  
  export default Header;