import React from 'react'
import './Ortho.css';
import chair_12 from '../Images/chair_1.png';
import chair_22 from '../Images/chair_2.png';
import chair_32 from '../Images/chair_3.png';
import chair_42 from '../Images/chair_4.png';
import chair_52 from '../Images/chair_5.png';

const Ortho = () => {
  return (
    <div className="ortho-page">
    <img src={chair_22} alt="" className="chair_42" />
    <img src={chair_12} alt="" className="chair_32" />
    <img src={chair_42} alt="" className="chair_12" />
    <img src={chair_52} alt="" className="chair_22" />
    <img src={chair_32} alt="" className="chair_52" />
  </div>
  )
}

export default Ortho
