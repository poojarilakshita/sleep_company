import React from 'react'
import './Shop_now.css'
// import video from '../Images/Shop_now.png'
import grid from '../Images/Smart_grid.png'
import smarter from '../Images/Smarter.png'
import ergonomic from '../Images/Ergonomic.png'
import google from '../Images/Google.png'
import amazon from '../Images/Amazon.png'
import bg3 from '../Images/bg3.png'
import videos from '../Images/Meet_India_s_1st.mp4'



const Shop_now = () => {
    const handleShopNowClick = () => {
        console.log('Shop Now button clicked!');
      };
  return (
    <div className='Container_3'>
      <img src={bg3} alt="" className="bg_13" />
      <video className='videos' width="100%" autoPlay loop muted controlsList="nofullscreen">
        <source src={videos} type="video/mp4" />
      </video>
      {/* <img src={video} alt="" className="video" /> */}
      <img src={grid} alt="" className="grid" />
      <img src={smarter} alt="" className="smarter" />
      <img src={ergonomic} alt="" className="ergonomic" />
      <p className="head">Just Smart sitting </p>
      <p className="head_2">nothing else</p>
      <p className="grid_text">Patented SmartGRID®</p>
      <p className="grid_text_2">Technology</p>
      <p className="smarter_text">Smarter</p>
      <p className="smarter_text_2">Designs</p>
      <p className="ergonomic_text">Ergonomic</p>
      <p className="ergonomic_text_2">Features</p>
      <button className= 'shop_now' type="button" onClick={handleShopNowClick}>Shop Now</button>
      <img src={google} alt="" className="google" />
      <img src={amazon} alt="" className="amazon" />

    </div>
  )
}

export default Shop_now
