import React, { useState } from 'react';
import './Compare.css';
import arrow from '../Images/down_arrow.png';
import arrow_1 from '../Images/down_arrow_2.png';
import grey from '../Images/grey.png';
import ultra from '../Images/ERGOSMART Ultra.png';
import max from '../Images/ERGOSMART Max.png';
import check from '../Images/right.png';
import snapmint from '../Images/snapmint.png';

const Compare = () => {
  const [isUltraActive, setIsUltraActive] = useState(false);
  const [isMaxActive, setIsMaxActive] = useState(false);

  const handleClick = (itemName, event) => {
    event.preventDefault();
    console.log(`Clicked on ${itemName}`);
// Toggle the state for the clicked chair
if (itemName === 'ERGOSMART Ultra') {
  setIsUltraActive(!isUltraActive);
} else if (itemName === 'ERGOSMART Max') {
  setIsMaxActive(!isMaxActive);
}
};

  return (
    <>
      <div className='compare'>
        <p className="text_4">COMPARE</p>
        <p className="text_5">CHAIRS</p>
        <div>
          <button type="button" className={`ERGOSMART_Ultra ${isUltraActive ? 'active' : ''}`}
 onClick={(e) => handleClick('ERGOSMART Ultra', e)}>
            ERGOSMART ULTRA
          </button>
          {isUltraActive ? (
    <img src={arrow} alt="" className="arrow" />
  ) : (
    <img src={arrow_1} alt="" className="arrow_1" />
  )}
          <button type="button" className={`ERGOSMART_Max ${isMaxActive ? 'active' : ''}`} onClick={(e) => handleClick('ERGOSMART Max', e)}>
            ERGOSMART MAX
          </button>
          {isMaxActive ? (
    <img src={arrow} alt="" className="arrow_2" />
  ) : (
    <img src={arrow_1} alt="" className="arrow_3" />
  )}
        </div>
        <div className="grey">
          {isUltraActive && (
            <>
              <img src={grey} alt="" className="chart" />
              <img src={ultra} alt="" className="grey_1" />
            </>
          )}
          {isMaxActive && (
            <>
              <img src={grey} alt="" className="chart_2" />
              <img src={max} alt="" className="grey_2" />
            </>
          )}
        </div>
      </div>

      {isUltraActive && (
        <div className="ultra_1" >
          <img src={check} alt="" className="check_1" />
        <p className="p1">Recline Up to 135⁰</p>
        <img src={check} alt="" className="check_2" />
        <p className="p2">Adjustable Backrest</p>
        <img src={check} alt="" className="check_3" />
        <p className="p3">3D Adjustable Armrests</p>
        <img src={check} alt="" className="check_4" />
        <p className="p4">3D Adjustable Headrest</p>
        <img src={check} alt="" className="check_5" />
        <p className="p5">Height Adjustable Seat</p>
        <img src={check} alt="" className="check_6" />
        <p className="p6">Extendable Footrest</p>
      </div>
      )}

      {isMaxActive && (
        <div className="max_1">
         <img src={check} alt="" className="check_7" />
        <p className="p7">Recline Up to 135⁰</p>
        <img src={check} alt="" className="check_8" />
        <p className="p8">Adjustable Lumbar</p>
        <img src={check} alt="" className="check_9" />
        <p className="p9">4D Adjustable Armrests</p>
        <img src={check} alt="" className="check_10" />
        <p className="p10">Adjustable Headrest</p>
        <img src={check} alt="" className="check_11" />
        <p className="p11">2D Adjustable Seat</p>
      </div>
      )}

      {isUltraActive && (
        <>
          <div className="dimension-ultra">
            <p className="text_6">Seat Dimensions</p>
            <p className="text_7">20 x 22 in (Width x Depth)</p>
          </div>

          <div className="height-ultra">
            <p className="text_10">Height Suitability</p>
            <p className="text_11">5’2” to 6’1” ft</p>
          </div>

          <div className="price-ultra">
            <p className="text_14">Offer Price</p>
            <p className="text_15">₹20,999</p>
            <p className="text_16">₹48,598</p>
            <p className="text_17">or Pay ₹5250 now. Rest in 0% interest EMI</p>
            <img src={snapmint} alt="" className="snapmint" />
            <button className='shop_now_7' type="button" onClick={(e) => handleClick("Shop Now", e)}>
              SHOP NOW
            </button>
          </div>
        </>
      )}

      {isMaxActive && (
        <>
          <div className="dimension-max">
            <p className="text_8">Seat Dimensions</p>
            <p className="text_9">20 x 20 in (Width x Depth)</p>
          </div>

          <div className="height-max">
            <p className="text_12">Height Suitability</p>
            <p className="text_13">5’2” to 6’1” ft</p>
          </div>

          <div className="price-max">
            <p className="text_18">Offer Price</p>
            <p className="text_19">₹22,999</p>
            <p className="text_20">₹41,598</p>
            <p className="text_21">or Pay ₹5750 now. Rest in 0% interest EMI</p>
            <img src={snapmint} alt="" className="snapmint_1" />
            <button className='shop_now_8' type="button" onClick={(e) => handleClick("Shop Now", e)}>
              SHOP NOW
            </button>
          </div>
        </>
      )}
    </>
  );
};

export default Compare;
