import React, { useState } from 'react';
import './Review.css';
import bg from '../Images/review_bg.png';
import right from '../Images/right-side-arrow.png';
import left from '../Images/left-side-arrow.png';
import review_1 from '../Images/review_1.png';
import review_2 from '../Images/review_2.png';
import review_3 from '../Images/review_3.png';
import review_4 from '../Images/review_4.png';

const Review = () => {
  const [activeReview, setActiveReview] = useState(1);

  const handleRightArrowClick = () => {
    setActiveReview((prevReview) => (prevReview % 4) + 1);
  };

  const handleLeftArrowClick = () => {
    setActiveReview((prevReview) => (prevReview === 1 ? 4 : prevReview - 1));
  };

  return (
    <div className='review'>
      <img src={bg} alt="" className="background" />
      <p className="head_7">TRUSTED BY 50K+ HAPPY CONSUMERS</p>
      <p className="head_8">See for yourself why our customers keep coming back!</p>
      <img src={right} alt="" className="right" onClick={handleRightArrowClick} />
      <img src={left} alt="" className="left" onClick={handleLeftArrowClick} />
      <div className="reviews">
      <img src={review_1} alt="" className={`review_1`} style={{ left: `${(activeReview - 1) * 330}px` }} />
      <img src={review_2} alt="" className={`review_2`} style={{ left: `${(activeReview % 4) * 330}px` }} />
      <img src={review_3} alt="" className={`review_3`} style={{ left: `${((activeReview + 1) % 4) * 330}px` }} />
      <img src={review_4} alt="" className={`review_4`} style={{ left: `${((activeReview + 2) % 4) * 330}px` }} />
      </div>
    </div>
  );
};

export default Review;
