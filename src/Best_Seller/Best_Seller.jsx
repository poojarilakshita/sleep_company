import React from 'react'
import './Best_Seller.css'
import best_seller_1 from '../Images/best_seller_1.png'
import best_seller_2 from '../Images/best_seller_2.png'
import best_seller_3 from '../Images/best_seller_3.png'

const Best_Seller = () => {
    const handleClick = (itemName, event) => {
        event.preventDefault();
        console.log(`Clicked on ${itemName}`);
    };
  return (
    <div className='best_seller'>
        <h1 className="top">BEST SELLERS</h1>
      <img src={best_seller_1} alt="" className="best_seller_1" />
      <button className= 'cart_2' type="button" onClick={(e) => handleClick("Added ERGOSMART Pro to the cart", e)}>ADD TO CART</button>
      <img src={best_seller_2} alt="" className="best_seller_2" />
      <button className= 'cart_3' type="button" onClick={(e) => handleClick("Added ERGOSMART Ultra to the cart", e)}>ADD TO CART</button>
      <img src={best_seller_3} alt="" className="best_seller_3" />
      <button className= 'cart_4' type="button" onClick={(e) => handleClick("Added ERGOSMART Gaming Pro to the cart", e)}>ADD TO CART</button>
    </div>
  )
}

export default Best_Seller
