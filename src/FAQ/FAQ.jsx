import React from 'react'
import './FAQ.css'

const FAQ = () => {
  return (
    <>
    <div className='FAQ'>
      <h1 className="head_23">FAQ</h1>
      <div className="Q_1"></div>
      <p className='p21'>What is the SmartGRID®?</p>

      <div className="Q_2"></div>
      <p className='p22'>Is SmartGRID® Soft or Firm?</p>

      <div className="Q_3"></div>
      <p className='p23'>How does SmartGRID® Feel?</p>

      <div className="Q_4"></div>
      <p className='p24'>Can SmartGRID® combat night sweats?</p>

      <div className="Q_5"></div>
      <p className='p25'>Is SmartGRID® Durable?</p>

      <div className="Q_6"></div>
      <p className='p26'>Is it Hypoallergenic?</p>

    </div>
    
  </>
  )
}

export default FAQ
