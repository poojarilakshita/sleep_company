import React from 'react'
import './Blogs.css'
import blogs from '../Images/Blogs.png'

const Blogs = () => {
  return (
    <>
    <div>
      <img src={blogs} alt="" className="blogs" />
    </div>
    <div className="text_99">
        <p className='text_100'>Are you an Ultra Achiever? Have you hustled all along in life? Then, this disruptive creation is surely yours, something that you have earned, something that you have always deserved! <br/>
        Inspired by human biological design and closely mimicking your spine, Ultron, a fusion of elegance and ergonomic brilliance, is here to elevate your comfort. The Sleep Company's commitment to innovation shines through in this ultra-premium office chair. With its Ultra Ergonomic Spine Protection, incredibly adjustable features and convenience more than you can imagine you needed,Ultron is surely a dream come true for you! <br/>
        Ultron: where comfort meets intelligence!</p>
    </div>
    </>
  )
}

export default Blogs
